##Shopping Cart For laravel 5 & 6

https://github.com/darryldecode/laravelshoppingcart

Go to this link and install pacakage following instruction. Then if you want to add item into cart so follow this:

## Step: 1 create CartController 
        php artisan make:controller CartController --resource

## Step: 2 create route 
    resource::(‘/cart’, ‘CartController’);

## Now Add this code in your product view page:
        
        <div class="addCart text-center"> 
           <a href="{{route('cart.edit',$product->id)}}"><i class="fas fa-shopping-cart mr-1"></i><strong>Add Cart</strong></a>
        </div>

## Then add your code into controller edit method:
        
        public function edit($id)
        {
           $product=Product::find($id);
           $cart= \Cart::add($id,$product->name,$product->price,1);
           return redirect()->route(‘cart’) ->withMessage('Item was added to your cart!');
        
}


Add cart view page in blade file 


## Next step :
If you want to update quantity so follow this :
        
        <table class="table table-hover">
           <thead>
           <tr>
               <th>Sl</th>
               <th>Name</th>
               <th>Price</th>
               <th>Quantity</th>
               <th>Action</th>
           </tr>
           </thead>
           <tbody>
           @foreach ($cartItems as $cartItem)
               <tr>
                   <th>1</th>
                   <th>{{$cartItem->name}}</th>
                   <form action="{{route('cart.update',$cartItem->id)}}" method="post">
                       @csrf
                       @method('put')
                   <th>{{$cartItem->price}}
                       <input type="text" name="price" hidden></th>
                   <th>
                           <input  name="quantity" type="text" value="{{$cartItem->quantity}}" class="form-control" style="width: 15%;font-size: 10px;    display: inline-block;">
                           <button type="submit" class="btn btn-success" style="    background: #ff7e46; font-size: 6px; border: none"><i class="fa fa-plus" ></i></button>
                   </th>
                   </form>
                   <form action="{{route('cart.destroy',$cartItem->id)}}" method="post">
                       @csrf
                       @method('delete')
                   <th>
                       <button class="fa" type="submit">
                           <a href=""><i class="fa fa-trash"></i></a>
                       </button>
                      </th>
                   </form>
               </tr>
           @endforeach
           <tr>
               <td></td>
               <td></td>
               <td></td>
               <td  class='font-weight-bold'>Total Items :</td>
               <td>{{\Cart::getTotalQuantity()}} Tk</td>
           </tr>
           <tr>
               <td></td>
               <td></td>
               <td></td>
               <td  class='font-weight-bold'>Sub Total :</td>
               <td>{{\Cart::getSubTotal()}} Tk</td>
           </tr>
           <tr>
               <td></td>
               <td></td>
               <td></td>
               <td class='font-weight-bold'>VAT  : </td>
               <td>5%</td>
           </tr>
           <tr>
               <td></td>
               <td></td>
               <td></td>
               <td class='font-weight-bold'>Total : </td>
               @php
               $total= \Cart::getTotal();
               $grandTotal = $total / 100 *(5);
        
               $allTotal =$total +$grandTotal;
               @endphp
               <td  class='font-weight-bold'>{{$allTotal}} Tk</td>
           </tr>
           </tbody>
        </table>


## Then add code into controller method :
        public function update(Request $request, $id)
         {
       
            $cartItem=  \Cart::update($id,array(
                        'quantity' => array(
                                'relative' => false,
                                'value' => $request->quantity,
                            ),
             ));
            Toastr::success('Cart Successfully Updated :)', 'Success');
            return redirect()->route('cart.index');
            
               Toastr::success('Cart Successfully Updated :)', 'Success');
           return redirect()->route('cart.index');
          }





## Remove Item from cart  :

    public function destroy($id)
    {
       \Cart::remove($id);
       Toastr::success('Cart Item Successfully Removed :)', 'Success');
       return redirect()->route('cart.index');
    }




